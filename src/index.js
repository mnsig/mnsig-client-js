require("regenerator-runtime/runtime");
require("isomorphic-fetch");
const querystring = require("querystring");
const sjcl = require("sjcl");
const ethtx = require("ethereumjs-tx");
const bitcoin = require("bitcoinjs-lib");
const coinselect = require("coinselect/split");
const coininfo = require("coininfo");

const BASE_APIHOST = "mnsig.com/server";

const SPLIT = {
  // Split if at least 0.15 BTC is available for outputs
  min: 0.15 * 1e8,
  // Split into 3 outputs
  num: 3
};

function LocalError(message, code) {
  this.message = message;
  this.statusMessage = this.message;
  this.statusCode = code;
  this.stack = new Error().stack;
  return this;
}
LocalError.prototype = Object.create(Error.prototype);
LocalError.prototype.name = "LocalError";

/**
 * Register other networks.
 */
function registerAlt() {
  coininfo.dash.main.versions.bip32 = {
    private: 0x2fe52cc,
    public: 0x2fe52f8
  };
  delete coininfo.dash.main.seedsDns;
  const dashmain = coininfo.dash.main.toBitcoinJS();
  dashmain.messagePrefix = '\x19DarkCoin Signed Message:\n'
  bitcoin.networks.dash = dashmain;

  coininfo.dash.test.versions.bip32 = {
    private: 0x3a805837,
    public: 0x3a8061a0
  };
  coininfo.dash.test.versions.public = 140;
  delete coininfo.dash.test.seedsDns;
  const dashtest = coininfo.dash.test.toBitcoinJS();
  dashtest.messagePrefix = dashmain.messagePrefix;
  bitcoin.networks.dashtest = dashtest;

  coininfo.litecoin.test.versions.bip32 = {
    private: 0x0436ef7d,
    public: 0x0436f6e1
  };
  coininfo.litecoin.test.versions.scripthash = 58;
  delete coininfo.litecoin.test.seedsDns;
  delete coininfo.litecoin.test.versions.scripthash2;
  const litetest = coininfo.litecoin.test.toBitcoinJS();
  litetest.messagePrefix = bitcoin.networks.litecoin.messagePrefix
  bitcoin.networks.litecointest = litetest;
}
registerAlt();


function pickNetwork(network) {
  let net = null;
  let coinType = null;
  switch (network) {
    case "bitcoin":
    case "bitcoin_segwit":
      net = bitcoin.networks.bitcoin;
      coinType = 0;
      break;
    case "litecoin":
      net = bitcoin.networks.litecoin;
      coinType = 2;
      break;
    case "dash":
      net = bitcoin.networks.dash;
      coinType = 5;
      break;
    case "ethereum":
      // bitcoin is used here only for deriving keys
      net = bitcoin.networks.bitcoin;
      coinType = 60;
      break;

    case "testnet":
    case "testbitcoin":
    case "testbitcoin_segwit":
    case "testethereum":
      net = bitcoin.networks.testnet;
      coinType = 1;  // same for all testnets
      break;
    case "testlitecoin":
    case "litecointest":
      net = bitcoin.networks.litecointest;
      coinType = 1;
      break;
    case "testdash":
    case "dashtest":
      net = bitcoin.networks.dashtest;
      coinType = 1;
      break;
  }
  if (!net) {
    throw new LocalError("unknown network", -400);
  }
  return { net: net, coinType: coinType };
}


/**
 * @param plainPassword   Plain text password to be used for encrypting the private key
 * @param derivation      Derivation method, null or "bip44"
 * @param network         "bitcoin" for the bitcoin mainnet or "testnet" for bitcoin testnet
 * @returns object containing "xpub" to be used my mnsig and "xprivEnc"
 */
function generateKey(plainPassword, derivation, network) {
  const { net, coinType } = pickNetwork(network);
  const randomKey = bitcoin.ECPair.makeRandom().d.toBuffer();
  const master = bitcoin.HDNode.fromSeedBuffer(randomKey, net);
  const enc = sjcl.encrypt(plainPassword, master.toBase58());
  let derived, xpub;

  if (derivation.toLowerCase() === "bip44") {
    const path = `m/44'/${coinType}'/0'`;
    derived = master.derivePath(path);
    xpub = derived.neutered().toBase58();
  } else {
    xpub = master.neutered().toBase58();
  }

  return {
    xpub: xpub.toString(),
    xprivEnc: enc
  };
}

/**
 * @param plainPassword   Plain text password to be used for decrypting the private key
 * @param xprivEnc        Encrypted private key
 * @param derivation      Derivation method, null or "bip44"
 * @param network         "bitcoin" for the bitcoin mainnet or "testnet" for bitcoin testnet
 * @returns a xpriv ready to be used by mnsig for locally signing
 */
function decryptKey(plainPassword, xprivEnc, derivation, network) {
  const { net, coinType } = pickNetwork(network);
  let xpriv, xprivRaw;

  try {
    xprivRaw = sjcl.decrypt(plainPassword, xprivEnc);
  } catch (e) {
    console.error(e);
    return null;
  }

  xpriv = bitcoin.HDNode.fromBase58(xprivRaw, net);
  if (derivation.toLowerCase() === "bip44") {
    const path = `m/44'/${coinType}'/0'`;
    return xpriv.derivePath(path);
  } else {
    return xpriv;
  }
}


function timeout(ms, promise) {
  return new Promise(function(resolve, reject) {
    setTimeout(function() {
      reject(new Error("timeout"));
    }, ms);
    promise.then(resolve, reject);
  });
}

/**
 * @param serverInstance  Server instance to use, string.
 * @param opts            Information about this client.
 * @param opts.token      Client token, string.
 * @param opts.host       Server host, string.
 * @param opts.split      UTXO split config.
 * @param opts.split.min  Minimum spare output amount to consider for splitting.
 * @param opts.split.num  Split spare output amount into num outputs.
 * @param opts.nosplit    Disable UTXO splitting.
 */
class MNSigClient {
  constructor(serverInstance, opts) {
    const options = opts || {};
    this.instance = serverInstance;
    this.host = options.host ? options.host : BASE_APIHOST;
    this.timeout = options.timeout !== undefined ? options.timeout : 15 * 1000;
    if (options.nosplit !== true) {
      this.split = options.split ? options.split : SPLIT;
    } else {
      this.split = null;
    }
    this.headers = Object.assign({}, { "User-Agent": "mnsig-client-0.2" });
    this.headers["Token"] = options.token;
    this.headersBody = Object.assign({}, this.headers, {
      "Content-Type": "application/json"
    });
  }

  get token() {
    return this.headers["Token"];
  }

  set token(newToken) {
    this.headers["Token"] = newToken;
    this.headersBody["Token"] = newToken;
  }

  async _request(path, params = null, method = "GET") {
    let url = `https://${this.instance}.${this.host}/${this.instance}/api/v1/${path}`;
    let opts = {
      headers: this.headers,
      method: method,
      mode: "cors"
    };
    if (params) {
      if (opts.method !== "GET") {
        opts.body = JSON.stringify(params);
        opts.headers = this.headersBody;
      } else {
        const qs = querystring.stringify(params);
        url = `${url}?${qs}`;
      }
    }

    let resp;
    try {
      resp = await timeout(this.timeout, fetch(url, opts));
    } catch (err) {
      const timedOut = err.message === "timeout";
      return {
        isError: true,
        data: { message: timedOut ? "Request timed out" : "Network Error" },
        code: timedOut ? -2 : -1
      };
    }

    const data = await resp.json();
    const result = {
      data: data,
      code: resp.status,
      isError: resp.status !== 200
    };
    return result;
  }

  /**
   * User data, used by web clients.
   */
  async userdata() {
    return await this._request("user/data");
  }

  /**
   * Signup is typically used only by web clients.
   *
   * @param opts            Information about the user that wants to sign up.
   * @param opts.username   The username as a string.
   * @param opts.hashed_pwd A non-plaintext version of the password.
   */
  async signup(opts) {
    return await this._request("user/signup", opts, "POST");
  }

  /**
   * Login is typically used only by web clients.
   *
   * @param opts            Information about the user that wants to log in.
   * @param opts.username   The username as a string.
   * @param opts.hashedpwd  A non-plaintext version of the password.
   * @param opts.totp       2FA totp code.
   */
  async login(opts) {
    const params = {
      username: opts.username,
      hashed_pwd: opts.hashedpwd,
      totp: opts.totp
    };
    let result = await this._request("user/login", params, "POST");
    if (result.isError && result.code === -1) {
      result.data.message = `Instance ${this.instance} is not accessible`;
    }
    return result;
  }

  /**
   * Logout is typically used only by web clients.
   */
  async logout() {
    return await this._request("user/logout");
  }

  /**
   * @param opts            Information about the wallet being created.
   * @param opts.wallet     Wallet name, string.
   * @param opts.type       Wallet type, "bitcoin" (bitcoin mainnet) and
   *                          "testnet" (bitcoin testnet) supported.
   * @param opts.m          Signatures required, positive integer.
   * @param opts.n          Number of signers, n >= m.
   * @param opts.derivation Derivation method, null or "bip44" (default).
   */
  async createWallet(opts) {
    const params = {
      wallet: opts.wallet,
      wallet_type: opts.type,
      forwarder: opts.forwarder,
      m: opts.m,
      n: opts.n
    };
    if (opts.derivation === undefined) {
      params.wallet_derivation = "bip44";
    } else if (opts.derivation !== null) {
      params.wallet_derivation = opts.derivation;
    }
    return await this._request("wallet/", params, "POST");
  }

  /**
   * Create a new API token.
   *
   * @param opts            Information about the wallet to add a token.
   * @param opts.wallet     Wallet name, string.
   * @param opts.label      Custom label for this token, a string
   * @param opts.maxAmount  Amount in BTC above which this token is disallowed
   *                          from sending signed proposals
   * @param opts.restrictIP Restrict access to this token to one or more IP
   *                          addresses or ranges, a list.
   *                          Example: ['8.8.4.4', '8.8.8.0/32']
   */
  async addToken(opts) {
    const params = {
      restrict_to: opts.restrictIP.join(","),
      max_txamount: opts.maxAmount,
      label: opts.label
    };
    return await this._request(`wallet/${opts.wallet}/token/`, params, "POST");
  }

  /**
   * @param opts            Information about the wallet to delete a token.
   * @param opts.wallet     Wallet name, string.
   * @param opts.tokenId    Token id to disable.
   */
  async deleteToken(opts) {
    return await this._request(
      `wallet/${opts.wallet}/token/${opts.tokenId}`,
      null,
      "DELETE"
    );
  }

  /**
   * @param opts            Information about the wallet to add an user.
   * @param opts.wallet     Wallet name, string.
   * @param opts.username   Username for the user to be added, string.
   * @param opts.perms      List of permissions, one or more of 'VIEW', 'DERIVE',
   *                          'SIGN'.
   */
  async addWalletUser(opts) {
    const params = {
      username: opts.username,
      permissions: opts.perms.join(",")
    };
    return await this._request(`wallet/${opts.wallet}/user`, params, "POST");
  }

  /**
   * @param opts            Information about the wallet being retrieved.
   * @param opts.wallet     Wallet name, string.
   */
  async walletInfo(opts) {
    return await this._request(`wallet/${opts.wallet}`);
  }

  /**
   * @param opts            Information about the wallet to retrieve keys.
   * @param opts.wallet     Wallet name, string.
   */
  async walletKeys(opts) {
    return await this._request(`wallet/${opts.wallet}/keys`);
  }

  /**
   * Add a key to an existing wallet.
   *
   * @param opts            Information about the key being added.
   * @param opts.wallet     Wallet name, string.
   * @param opts.xpub       xpub that will be used for deriving addresses
   *                          on the server, a string.
   * @param opts.encxpriv   Encrypted xpriv to be retrieved when signing,
   *                          optional.
   */
  async addWalletKey(opts) {
    const params = {
      wallet: opts.wallet,
      xpub: opts.xpub,
      encrypted_xpriv: opts.encxpriv || null
    };
    return await this._request("wallet/", params, "PUT");
  }

  /**
   * @param opts            Information about the wallet to retrieve balance.
   * @param opts.wallet     Wallet name, string.
   * @param opts.target     Number of confirmations to consider for confirmed
   *                          balance, default 2.
   */
  async balance(opts) {
    let params = null;
    if (opts.target) {
      params = {
        target: opts.target
      };
    }
    return await this._request(`wallet/${opts.wallet}/balance`, params, "GET");
  }

  /**
   * @param opts            Information about the wallet to derive an address.
   * @param opts.wallet     Wallet name, string.
   * @param opts.change     Derive an internal (change) address? Boolean.
   */
  async newAddress(opts) {
    let params = {};
    if (opts.change !== undefined) {
      params.change = opts.change;
    }
    return await this._request(`wallet/${opts.wallet}/address`, params, "POST");
  }

  /**
   * @param opts            Information about the wallet to retrieve transactions.
   * @param opts.wallet     Wallet name, string.
   * @param opts.export     Return transactions as CSV, boolean (default false).
   * @param opts.minTime    Return transactions with a complete date equal or
   *                          greater to this number, unix timestamp.
   * @param opts.maxTime    Return transactions with a complete date less than
   *                          this number, unix timestamp.
   * @param opts.page       Page bookmark, string.
   */
  async txlist(opts) {
    let params = null;
    if (opts.export || opts.minTime || opts.maxTime || opts.page) {
      params = {};
      if (opts.export) params.export = 1;
      if (opts.minTime) params.min_ts = opts.minTime;
      if (opts.maxTime) params.max_ts = opts.maxTime;
      if (opts.page) params.page = opts.page;
    }
    return await this._request(
      `wallet/${opts.wallet}/transactions`,
      params,
      "GET"
    );
  }

  /**
   * @param opts            Information about the wallet to retrieve one transaction.
   * @param opts.wallet     Wallet name, string.
   * @param opts.txid       Bitcoin transaction id.
   * @param opts.extid      External id associated to the transaction be returned.
   */
  async txget(opts) {
    let params = null;
    if (opts.txid || opts.extid) {
      params = {};
      if (opts.txid) params.txid = opts.txid;
      else if (opts.extid) params.external_id = opts.extid;
    }
    return await this._request(
      `wallet/${opts.wallet}/transaction`,
      params,
      "GET"
    );
  }

  /**
   * @param opts            Information about the wallet being queried.
   * @param opts.wallet     Wallet name, string.
   */
  async listUTXOs(opts) {
    return await this._request(`wallet/${opts.wallet}/utxos`);
  }

  /**
   * @param opts            Information about the proposal being created.
   * @param opts.wallet     Wallet name, string.
   * @param opts.dest       Destination address, string.
   * @param opts.amount     Amount in BTC, string/number, e.g. '0.42'
   * @param opts.note       Custom note about this proposal, optional string
   */
  async startProposal(opts) {
    return await this._request("transaction/proposal/", opts, "POST");
  }

  /**
   * @param opts            Information about the sendmany proposal being created.
   * @param opts.wallet     Wallet name, string.
   * @param opts.recipients Recipients, a list of {<address, string> : <amount, string/number}
   * @param opts.note       Custom note about this proposal, optional string
   */
  async startManyProposal(opts) {
    return await this._request("transaction/proposal/many", opts, "POST");
  }

  /**
   * @param opts            Information about the hotwallet proposal being created.
   */
  async hotwalletProposal(opts) {
    return await this._request("transaction/hotwallet/", opts, "POST");
  }

  /**
   * @param opts            Information about the proposal being returned.
   * @param opts.wallet     Wallet name, string.
   * @param opts.id         Proposal id, integer.
   */
  async getProposal(opts) {
    return await this._request(
      `transaction/${opts.wallet}/proposal/${opts.id}`
    );
  }

  /**
   * @param opts            Information about the proposal being canceled.
   * @param opts.wallet     Wallet name, string.
   * @param opts.id         Proposal id, integer.
   */
  async deleteProposal(opts) {
    return await this._request(
      `transaction/${opts.wallet}/proposal/${opts.id}`,
      null,
      "DELETE"
    );
  }

  /**
   * @param opts            Information about the proposals being returned.
   * @param opts.wallet     Wallet name, string.
   * @param opts.state      Return proposals in this state, string. One of
   *                          one of: "no_signatures", "partial", "canceled", or
   *                          "complete".
   */
  async getProposalsByState(opts) {
    return await this._request(
      `transaction/${opts.wallet}/proposal/${opts.state}`
    );
  }

  /**
   * @param opts            Information about the wallet to estimate fees.
   * @param opts.wallet     Wallet name, string.
   * @param opts.target     Block target, integer.
   */
  async estimateFee(opts) {
    let target = 2;
    if (opts.target !== undefined) {
      target = opts.target;
    }
    return await this._request(`wallet/${opts.wallet}/estimatefee/${target}`);
  }

  /**
   * @param opts            Information about the bitcoin transaction being queried.
   * @param opts.wallet     Wallet name, string.
   * @param opts.txid       Transaction id, string.
   */
  async txinfo(opts) {
    return await this._request(`transaction/${opts.wallet}/${opts.txid}`);
  }

  /**
   * @param proposal        Proposal returned from startProposal or getProposal.
   */
  async prepareProposal(proposal) {
    if (proposal.state !== "unprepared") {
      return {
        isError: true,
        code: -400,
        data: {
          message: "Unexpected proposal state"
        }
      };
    }

    const { net } = pickNetwork(proposal.wallet_type);
    const changeAddy = proposal.change_address.address;
    let amountIn = 0;
    let amountOut = 0;
    const utxos = [];
    const targets = [];
    proposal.utxo.forEach(function(u) {
      const value = Math.round(u.amount * 1e8);
      amountIn += value;
      u.script = Buffer(u.redeemScript, 'hex');
      u.value = value;
      utxos.push(u);
    });
    if (!proposal.recipients) {
      proposal.recipients = {};
      proposal.recipients[proposal.dest_address] = proposal.amount;
    }
    Object.keys(proposal.recipients).forEach(function(addy) {
      const amount = proposal.recipients[addy];
      amountOut += amount;
      targets.push({ address: addy, value: amount });
    });

    let fee = null;
    let spare = amountIn - amountOut;

    if (proposal.manual_fee) {
      // Manual fee specified, add a single change output with everything
      // that is left after this fee.
      fee = Math.round(proposal.manual_fee * 1e8);
      spare -= fee;
      targets.push({ address: changeAddy, value: spare });
      spare = 0;
    }

    const split = this.split;
    if (split && spare >= split.min) {
      const reserve = 0.01 * 1e8;
      const splitAmount = Math.floor((spare - reserve) / split.num);
      if (splitAmount >= 0.02 * 1e8) {
        /* Add extra outputs. */
        for (let i = 0; i < split.num; i++) {
          // Let coinselect+split set a value for this output.
          targets.push({ address: changeAddy });
        }
      }
    }

    let inputs, outputs;
    if (fee !== null) {
      // Manual fee specified, nothing left to do here.
      inputs = utxos;
      outputs = targets;
    } else {
      const feeRate = Math.round(proposal.fee_rate * 1e8 / 1000);
      const selectRes = coinselect(utxos, targets, feeRate);
      inputs = selectRes.inputs;
      outputs = selectRes.outputs;
      fee = selectRes.fee;
    }

    console.log(proposal.id, 'total fee', fee);
    const ptx = new bitcoin.TransactionBuilder(net);
    inputs.forEach(inp => ptx.addInput(inp.txid, inp.vout));
    outputs.forEach(out => {
      if (!out.address) {
        out.address = changeAddy;
      }
      ptx.addOutput(out.address, out.value);
    });

    const rawtx = ptx.buildIncomplete().toHex();
    const opts = {
      rawtx: rawtx,
      proposal_id: proposal.id,
      wallet: proposal.wallet
    };
    if (proposal.donotsend) {
      return opts;
    }
    return await this._rawPrepareProposal(opts);
  }

  async _rawPrepareProposal(opts) {
    return await this._request("transaction/proposal/", opts, "PUT");
  }

  /**
   * @param proposal        Proposal with 0 or more signatures.
   * @param xprivRaw        Private key used to locally sign.
   */
  async localSign(proposal, xprivRaw) {
    const { net } = pickNetwork(proposal.wallet_type);
    const xpriv = bitcoin.HDNode.fromBase58(xprivRaw, net);
    const xpub = xpriv.neutered().toBase58();

    let result
    if (proposal.wallet_type.indexOf("ethereum") >= 0) {
      result = await this._localSignEthereum(proposal, xpriv);
    } else {
      result = await this._localSign(proposal, xpriv, net);
    }

    const opts = {
      xpub: xpub,
      rawtx: result.rawtx,
      nsigs: result.nsigs,
      wallet: proposal.wallet,
      proposal_id: proposal.id
    };
    if (proposal.donotsend) {
      return opts;
    }
    return await this._rawSignProposal(opts);
  }

  async _localSign(proposal, xpriv, net) {
    const threshold = proposal.m;
    const utxo = proposal.utxo;
    const isSegwit = proposal.wallet_type.indexOf("_segwit") >= 0;

    const rawtx = bitcoin.Transaction.fromHex(proposal.rawtx);
    const ptx = new bitcoin.TransactionBuilder.fromTransaction(rawtx, net);

    const xpubList = proposal.xpub_list.map(function(p) {
      return bitcoin.HDNode.fromBase58(p, net);
    });

    utxo.forEach((u, indx) => {
      let path = u.path;
      if (path.indexOf("m/") === 0) {
        // In a previous version all paths started with m/ even though
        // they were not coming from a master node.
        // bitcoinjs-lib rejects those.
        path = path.slice(2)
      }

      const pubs = xpubList.map(function(x) {
        return x.derivePath(path).getPublicKeyBuffer();
      }).sort(Buffer.compare);

      const pk = xpriv.derivePath(path).keyPair;
      const value = Math.round(u.amount * 1e8);
      if (isSegwit) {
        const redeemScript = Buffer(u.redeemScript, "hex");
        const witnessScript = bitcoin.script.multisig.output.encode(threshold, pubs);
        ptx.sign(indx, pk, redeemScript, null, value, witnessScript);
      } else {
        ptx.sign(indx, pk, Buffer(u.redeemScript, "hex"), null, value);
      }
    });

    const sigcount = ptx.inputs[0].signatures.filter(v => v !== undefined).length;
    const complete = sigcount >= threshold;
    let rawresult;
    if (complete) {
      rawresult = ptx.build().toHex();
    } else {
      rawresult = ptx.buildIncomplete().toHex();
    }

    return {
      rawtx: rawresult,
      nsigs: sigcount
    };
  }

  async _localSignEthereum(proposal, xpriv) {
    const pk = xpriv.derivePath(proposal.utxo[0].path).keyPair;
    const tx = new ethtx(proposal.rawtx);
    if (proposal.wallet_type === "ethereum") {
      tx._chainId = 1;
    } else {
      tx._chainId = 3;
    }
    tx.sign(pk.d.toBuffer());
    const rawresult = tx.serialize().toString("hex");

    return {
      rawtx: rawresult,
      nsigs: 1
    };
  }

  async _rawSignProposal(opts) {
    return await this._request("transaction/proposal/sign", opts, "POST");
  }

  /**
   * @param proposal        Proposal in the complete state.
   */
  async broadcast(proposal) {
    if (proposal.wallet_type.indexOf("ethereum") < 0) {
      const err = this._checkBroadcast(proposal);
      if (err) {
        return err;
      }
    }

    const opts = {
      raw: proposal.rawtx,
      wallet: proposal.wallet,
      proposal_id: proposal.id
    };
    if (proposal.donotsend) {
      return opts;
    }
    return await this._rawBroadcast(opts);
  }

  _checkBroadcast(proposal) {
    const { net } = pickNetwork(proposal.wallet_type);
    const rawtx = bitcoin.Transaction.fromHex(proposal.rawtx);
    const ptx = new bitcoin.TransactionBuilder.fromTransaction(rawtx, net);
    const threshold = proposal.m;

    const sigcount = Math.min.apply(
      null,
      ptx.inputs.map(inp => inp.signatures.filter(v => v !== undefined).length)
    );
    if (sigcount < threshold) {
      return {
        isError: true,
        code: -400,
        data: {
          message: "Transaction not fully signed yet"
        }
      };
    }

    const rawcheck = ptx.build().toHex();
    if (rawcheck !== proposal.rawtx) {
      return {
        isError: true,
        code: -400,
        data: {
          message: "Transaction encoding mistmatch"
        }
      };
    }
  }

  async _rawBroadcast(opts) {
    return await this._request("transaction/broadcast", opts, "POST");
  }
}
// MNSigClient end

module.exports = {
  MNSigClient: MNSigClient,
  generateKey: generateKey,
  decryptKey: decryptKey
};
