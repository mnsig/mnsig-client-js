const crypto = require("crypto");
const assert = require("chai").assert;
const record = require("./record");
const Client = require("../src").MNSigClient;

describe("Client", function() {

  const recorder = record("mnsig");
  before(recorder.before);

  describe("bad server requests", function() {
    it("should fail with ENOTFOUND", function(done) {
      const cli = new Client("", {"host": "https://doesnotexist.mnsig.com"});
      cli.userdata().then(response => {
        assert.isTrue(response.isError);
        assert.equal(response.code, -1);
        assert.equal(response.data.message, "Network Error");
        done();
      });
    });

    it("should fail with 404 for an instance that does not exist", function(done) {
      const cli = new Client("np");
      cli.userdata().then(response => {
        assert.isTrue(response.isError);
        assert.equal(response.code, 404);
        assert.equal(response.data.message, "Not found");
        done();
      });
    });

    it("should fail with 400 since no token was specified", function(done) {
      const cli = new Client("gpg");
      cli.userdata().then(response => {
        assert.isTrue(response.isError);
        assert.equal(response.code, 400);
        assert.equal(response.data.message.token, "Specify your user token");
        done();
      });
    });

    it("should fail with 401 since the token is invalid", function(done) {
      const cli = new Client("gpg", {"token": "hello"});
      cli.userdata().then(response => {
        assert.isTrue(response.isError);
        assert.equal(response.code, 401);
        assert.equal(response.data.message, "Invalid token");
        done();
      });
    });
  });


  describe("user access", function() {
    // These tests assume a pre-existing user under a certain state.
    let token;

    it("should be able to log in", function(done) {
      const cli = new Client("gpg");
      const pwd = "qweasdzxczxc";
      const hashedPwd = crypto.createHash("sha256").update(pwd).digest("hex");
      const data = {"username": "someuser", "hashedpwd": hashedPwd};
      cli.login(data).then(response => {
        assert.isFalse(response.isError);
        assert.equal(response.data.welcome, data.username);
        token = response.data.token;
        done();
      });
    });

    it("should be able to access its data", function(done) {
      const cli = new Client("gpg", {"token": token});
      cli.userdata().then(response => {
        assert.isFalse(response.isError);
        assert.equal(response.data.wallets.length, 3);

        // It is known that the first wallet (2-of-3) in this case is in use.
        const wallet = response.data.wallets[0];
        assert.equal(wallet.complete, true);
        assert.equal(wallet.configured, true);
        assert.equal(wallet.num_keys, 3);
        assert.equal(wallet.m, 2)
        assert.equal(wallet.n, wallet.num_keys);
        assert.equal(wallet.derivation, "bip44");
        assert.equal(wallet.type, "testnet");
        assert.equal(wallet.balance.confirmed, 0.77);
        assert.equal(wallet.balance.total, 0.77);
        assert.equal(wallet.name, "my first wallet");
        assert.equal(wallet.pending_proposals.length, 0);
        assert.equal(wallet.users.length, 2);
        assert.equal(wallet.users[0].username, "someuser");
        assert.equal(wallet.users[1].username, "otheruser");
        assert.equal(wallet.users[1].permissions[0], "derive");
        assert.equal(wallet.api_tokens.length, 1);
        assert.equal(wallet.api_tokens[0].max_txamount, 1100000000);
        assert.equal(wallet.api_tokens[0].label, "remote1");
        assert.equal(wallet.api_tokens[0].last_request_at, null);
        assert.deepEqual(wallet.last_address,
                         {path: "m/1/18",
                          address: "2NCh8qeMFvP5dsG4sbdqXQ8FFkMgtrTQmoH"});

        // The second wallet (1-of-2) is not ready.
        const wallet1 = response.data.wallets[1];
        assert.equal(wallet1.complete, false);
        assert.equal(wallet1.configured, false);
        assert.equal(wallet1.num_keys, 1);
        assert.equal(wallet1.m, 1);
        assert.equal(wallet1.n, 2);
        assert.equal(wallet1.derivation, "bip44");
        assert.equal(wallet1.type, "testnet");
        assert.equal(wallet1.balance, null);
        assert.equal(wallet1.name, "another one");
        assert.equal(wallet1.pending_proposals.length, 0);
        assert.equal(wallet1.users.length, 2);
        assert.deepEqual(wallet1.users[0],
                         {username: "someuser", permissions: ["admin"]});
        assert.equal(wallet1.api_tokens.length, 0);
        assert.deepEqual(wallet1.last_address, {});
        assert.deepEqual(wallet1.last_tx, {});

        done();
      });
    });

    it("should be able to see its completed transactions", function(done) {
      const cli = new Client("gpg", {"token": token});
      cli.txlist({wallet: "my first wallet"}).then(response => {
        assert.isFalse(response.isError);
        assert.equal(response.data.filtered_count, 1);
        assert.equal(response.data.total_count, 1);
        assert.equal(response.data.data.length, 1);
        assert.equal(response.data.data[0].confirmed, true);
        assert.equal(response.data.data[0].amount, 77000000);
        done();
      });
    });

    it("should be able to create and cancel a proposal", function(done) {
      const cli = new Client("gpg", {"token": token, "nosplit": true});
      const data = {
        "wallet": "my first wallet",
        "dest": "2NASwd1heRe2Wd6JJdeKHBtEVL8qGVSc4SJ",
        "amount": "0.01",
        "note": "nothing to see here"
      };
      //cli.deleteProposal({id: 20, wallet: data.wallet}).then(response => {
      //  assert.equal(response.data.id, 20);
      //  assert.equal(response.data.state, "canceled");
      //  done();
      //});
      //return;
      cli.startProposal(data).then(response => {
        assert.isFalse(response.isError);
        assert.equal(response.data.state, "unprepared");
        const ddata = {"id": response.data.id, "wallet": response.data.wallet};

        cli.prepareProposal(response.data).then(presult => {
          assert.isFalse(presult.isError);
          assert.equal(presult.data.state, "no_signatures");
          assert.equal(presult.data.updated, true);

          cli.deleteProposal(ddata).then(dresult => {
            assert.isFalse(dresult.isError);
            assert.equal(dresult.data.state, "canceled");
            assert.equal(dresult.data.id, ddata.id);
            done();
          });
        });
      });
    });

    it("should be able to log out", function(done) {
      const cli = new Client("gpg", {"token": token});
      cli.logout().then(response => {
        assert.isFalse(response.isError);
        // Token is no longer valid.
        assert.equal(response.data.success, true);
        done();
      });
    });

    it("should not be able to get data with an expired token", function(done) {
      const cli = new Client('gpg', {'token': token});
      cli.userdata().then(response => {
        assert.isTrue(response.isError);
        assert.equal(response.code, 401);
        assert.equal(response.data.message, 'Invalid token');
        done();
      });
    });

  });

  after(recorder.after);
});
