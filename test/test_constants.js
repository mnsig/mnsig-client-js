const assert = require("chai").assert;
const bitcoin = require("bitcoinjs-lib");
require("../src");  // load mnsig-client in order to run registerAlt()

const EXPECTED = '{"bitcoin":{"messagePrefix":"\\u0018Bitcoin Signed Message:\\n","bech32":"bc","bip32":{"public":76067358,"private":76066276},"pubKeyHash":0,"scriptHash":5,"wif":128},"testnet":{"messagePrefix":"\\u0018Bitcoin Signed Message:\\n","bech32":"tb","bip32":{"public":70617039,"private":70615956},"pubKeyHash":111,"scriptHash":196,"wif":239},"litecoin":{"messagePrefix":"\\u0019Litecoin Signed Message:\\n","bip32":{"public":27108450,"private":27106558},"pubKeyHash":48,"scriptHash":50,"wif":176},"dash":{"hashGenesisBlock":"00000ffd590b1485b3caadc19b22e6379c733355108f107a430458cdf3407ab6","port":9999,"portRpc":9998,"protocol":{"magic":3177909439},"versions":{"bip32":{"private":50221772,"public":50221816},"bip44":5,"private":204,"public":76,"scripthash":16},"name":"Dash","unit":"DASH","testnet":false,"messagePrefix":"\\u0019DarkCoin Signed Message:\\n","bip32":{"public":50221816,"private":50221772},"pubKeyHash":76,"scriptHash":16,"wif":204,"dustThreshold":null},"dashtest":{"hashGenesisBlock":"00000bafbc94add76cb75e2ec92894837288a481e5c005f6563d91623bf8bc2c","port":19999,"portRpc":19998,"versions":{"bip32":{"private":981489719,"public":981492128},"bip44":1,"private":239,"public":140,"scripthash":19},"name":"Dash","unit":"DASH","testnet":true,"messagePrefix":"\\u0019DarkCoin Signed Message:\\n","bip32":{"public":981492128,"private":981489719},"pubKeyHash":140,"scriptHash":19,"wif":239,"dustThreshold":null},"litecointest":{"hashGenesisBlock":"f5ae71e26c74beacc88382716aced69cddf3dffff24f384e1808905e0188f68f","versions":{"bip44":1,"private":239,"public":111,"scripthash":58,"bip32":{"private":70709117,"public":70711009}},"name":"Litecoin","unit":"LTC","testnet":true,"messagePrefix":"\\u0019Litecoin Signed Message:\\n","bip32":{"public":70711009,"private":70709117},"pubKeyHash":111,"scriptHash":58,"wif":239,"dustThreshold":null}}';


describe("Constants", function() {

  describe("network constants", function() {
    it("should match", function() {
      assert.equal(EXPECTED, JSON.stringify(bitcoin.networks));
    });
  });

});
