var nock = require('nock');

nock('https://np.mnsig.com:443', {"encodedQueryParams":true})
  .get('/server/np/api/v1/user/data')
  .reply(404, {"message":"Not found"}, { server: 'nginx/1.8.1',
  date: 'Thu, 19 May 2016 01:49:57 GMT',
  'content-type': 'application/json',
  'content-length': '24',
  connection: 'close' });


nock('https://gpg.mnsig.com:443', {"encodedQueryParams":true})
  .get('/server/gpg/api/v1/user/data')
  .reply(400, {"message":{"token":"Specify your user token"}}, { server: 'nginx/1.8.1',
  date: 'Thu, 19 May 2016 01:49:57 GMT',
  'content-type': 'application/json',
  'content-length': '50',
  connection: 'close' });


nock('https://gpg.mnsig.com:443', {"encodedQueryParams":true})
  .get('/server/gpg/api/v1/user/data')
  .reply(401, {"message":"Invalid token"}, { server: 'nginx/1.8.1',
  date: 'Thu, 19 May 2016 01:49:58 GMT',
  'content-type': 'application/json',
  'content-length': '29',
  connection: 'close' });


nock('https://gpg.mnsig.com:443', {"encodedQueryParams":true})
  .post('/server/gpg/api/v1/user/login', {"username":"someuser","hashed_pwd":"25c132530261d0fdf223af2058c985cfce9b781633778a3c8044ee4d79008ef6"})
  .reply(200, {"token":"a5f571d9e42c4d80b9f28cc11e0fb93e","welcome":"someuser","success":true}, { server: 'nginx/1.8.1',
  date: 'Thu, 19 May 2016 01:49:59 GMT',
  'content-type': 'application/json',
  'content-length': '86',
  connection: 'close',
  'strict-transport-security': 'max-age=15768000' });


nock('https://gpg.mnsig.com:443', {"encodedQueryParams":true})
  .get('/server/gpg/api/v1/user/data')
  .reply(200, {"wallets":[{"complete":true,"configured":true,"num_keys":3,"pending_proposals":[],"users":[{"username":"someuser","permissions":["admin"]},{"username":"otheruser","permissions":["derive","view"]}],"n":3,"last_address":{"path":"m/1/18","address":"2NCh8qeMFvP5dsG4sbdqXQ8FFkMgtrTQmoH"},"api_tokens":[{"id":135,"last_request_at":null,"max_txamount":1100000000,"label":"remote1"}],"last_tx":{"note":null,"proposal_id":null,"amount":77000000,"dest_address":"2N5KzeYMyhk35BubM5YScfCJwcdmn1kQmtA","txid":"ad6ccab5cf0456138b0efd44555881b838c065163859f897a6d831f23e0b1ddb"},"derivation":"bip44","m":2,"name":"my first wallet","balance":{"confirmed":0.77,"total":0.77},"type":"testnet"},{"complete":false,"configured":false,"num_keys":1,"pending_proposals":[],"users":[{"username":"someuser","permissions":["admin"]},{"username":"otheruser","permissions":["view"]}],"n":2,"last_address":{},"api_tokens":[],"last_tx":{},"derivation":"bip44","m":1,"name":"another one","balance":null,"type":"testnet"},{"complete":false,"configured":false,"num_keys":0,"pending_proposals":[],"users":[{"username":"someuser","permissions":["admin"]}],"n":3,"last_address":{},"api_tokens":[],"last_tx":{},"derivation":"bip44","m":1,"name":"something","balance":null,"type":"testnet"}],"success":true}, { server: 'nginx/1.8.1',
  date: 'Thu, 19 May 2016 01:49:59 GMT',
  'content-type': 'application/json',
  'content-length': '1397',
  connection: 'close',
  'strict-transport-security': 'max-age=15768000' });


nock('https://gpg.mnsig.com:443', {"encodedQueryParams":true})
  .get('/server/gpg/api/v1/wallet/my%20first%20wallet/transactions')
  .reply(200, {"data":[{"confirmed":true,"dest_address":"2N5KzeYMyhk35BubM5YScfCJwcdmn1kQmtA","txid":"ad6ccab5cf0456138b0efd44555881b838c065163859f897a6d831f23e0b1ddb","note":null,"amount":77000000,"proposal_id":null,"complete_date":"2016-04-26T03:01:13"}],"total_count":1,"next_ids":[],"success":true,"filtered_count":1}, { server: 'nginx/1.8.1',
  date: 'Thu, 19 May 2016 01:50:00 GMT',
  'content-type': 'application/json',
  'content-length': '330',
  connection: 'close',
  'strict-transport-security': 'max-age=15768000' });


nock('https://gpg.mnsig.com:443', {"encodedQueryParams":true})
  .post('/server/gpg/api/v1/transaction/proposal/', {"wallet":"my first wallet","dest":"2NASwd1heRe2Wd6JJdeKHBtEVL8qGVSc4SJ","amount":"0.01","note":"nothing to see here"})
  .reply(200, {"utxo":[{"redeemScript":"5221021383288bd28eaaec7f187f1b6a627c5f20e9ab48206f7284ee7d17a28669eefa2103a9ba00954d7254d232a297428848c37e8969f88deca1870dfbcf82324ad3c9382103df2fb7f03fb86ca84276d740627101a3b118c923e306db0f81c9e67d1e45bbe053ae","account":"","vout":0,"txid":"ad6ccab5cf0456138b0efd44555881b838c065163859f897a6d831f23e0b1ddb","amount":0.77,"confirmations":41980,"address":"2N5KzeYMyhk35BubM5YScfCJwcdmn1kQmtA","spendable":false,"path":"m/0/0","scriptPubKey":"a914848874064118b6ad501007ab2bcf538e462209a687"}],"xpub_list":["tpubDCYeHFHeTsUYty8q2r8eJH5EwvmVPqvGJsuK7TcGjgwCrdiuQZF6y9yTuhuAmuzXsGH3nH8hWHXhrCXMhF1PKD9RZXZ9Qs13cJBLqkgErUc","tpubDCVpXNRk6QXK8J7R14aZDo5WCczxd4syFKzBDueizjNsXC1bUz5meM8u3KQYtUuL5Abec5zsJBp39FcE4RjZjbb1rGLFAzi61yMjm64jGhT","tpubDDAjaCC11MWamCtakSRUqoYaeg5ccirjCb9VgdAUfaP2kniSn1dcdavh64bUAUjBRK39JH2YwCoSTXprdrZZrg618VKFMwaxZhKL1cwAXdH"],"success":true,"dest_address":"2NASwd1heRe2Wd6JJdeKHBtEVL8qGVSc4SJ","created_at":"2016-05-18T21:50:00.631359","m":2,"amount":1000000,"created_by":"someuser","note":"nothing to see here","wallet":"my first wallet","wallet_type":"testbitcoin","fee_rate":0.00060,"state":"unprepared","id":20,"change_address":{"path":"m/1/19","script":"5221020fceb2e8d3436f2cb4913361244785afd92fde0e66c30cd250b923ebc34188ca21036b03596b4847efbcc8e33056d67f6438f01b19999e91b3c5819a5469a2f4425f2103b671696ccd7615fe665de7e6e1d684872283ad14553386f5075e4df45801b6d953ae","address":"2N8hw66DB7ASEP2VY9ehUgNhqAcYSESEWAE"}}, { server: 'nginx/1.8.1',
  date: 'Thu, 19 May 2016 01:50:00 GMT',
  'content-type': 'application/json',
  'content-length': '1474',
  connection: 'close',
  'strict-transport-security': 'max-age=15768000' });


nock('https://gpg.mnsig.com:443', {"encodedQueryParams":true})
  .put('/server/gpg/api/v1/transaction/proposal/', {"rawtx": "0100000001db1d0b3ef231d8a697f859381665c038b881585544fd0e8b135604cfb5ca6cad0000000000ffffffff0240420f000000000017a914bcb177b47408fec772b74843ad724a77a5d8dc4487807687040000000017a914a99702aa5775769f1f9c2ed13c74b94bbd2babe08700000000","proposal_id":20,"wallet":"my first wallet"})
  .reply(200, {"proposal_id":20,"updated":true,"state":"no_signatures","success":true}, { server: 'nginx/1.8.1',
  date: 'Thu, 19 May 2016 01:50:01 GMT',
  'content-type': 'application/json',
  'content-length': '80',
  connection: 'close',
  'strict-transport-security': 'max-age=15768000' });


nock('https://gpg.mnsig.com:443', {"encodedQueryParams":true})
  .delete('/server/gpg/api/v1/transaction/my%20first%20wallet/proposal/20')
  .reply(200, {"state":"canceled","id":20,"success":true}, { server: 'nginx/1.8.1',
  date: 'Thu, 19 May 2016 01:50:01 GMT',
  'content-type': 'application/json',
  'content-length': '49',
  connection: 'close',
  'strict-transport-security': 'max-age=15768000' });


nock('https://gpg.mnsig.com:443', {"encodedQueryParams":true})
  .get('/server/gpg/api/v1/user/logout')
  .reply(200, {"success":true}, { server: 'nginx/1.8.1',
  date: 'Thu, 19 May 2016 01:50:02 GMT',
  'content-type': 'application/json',
  'content-length': '18',
  connection: 'close',
  'strict-transport-security': 'max-age=15768000' });


nock('https://gpg.mnsig.com:443', {"encodedQueryParams":true})
  .get('/server/gpg/api/v1/user/data')
  .reply(401, {"message":"Invalid token"}, { server: 'nginx/1.8.1',
  date: 'Thu, 19 May 2016 01:50:02 GMT',
  'content-type': 'application/json',
  'content-length': '29',
  connection: 'close' });
